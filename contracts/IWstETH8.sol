// Sources flattened with hardhat v2.19.3 https://hardhat.org

// SPDX-License-Identifier: GPL-3.0 AND MIT

// File @openzeppelin/contracts/token/ERC20/IERC20.sol@v5.0.1

// Original license: SPDX_License_Identifier: MIT
// OpenZeppelin Contracts (last updated v5.0.0) (token/ERC20/IERC20.sol)

pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 standard as defined in the EIP.
 */
interface IERC20 {
    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev Emitted when the allowance of a `spender` for an `owner` is set by
     * a call to {approve}. `value` is the new allowance.
     */
    event Approval(address indexed owner, address indexed spender, uint256 value);

    /**
     * @dev Returns the value of tokens in existence.
     */
    function totalSupply() external view returns (uint256);

    /**
     * @dev Returns the value of tokens owned by `account`.
     */
    function balanceOf(address account) external view returns (uint256);

    /**
     * @dev Moves a `value` amount of tokens from the caller's account to `to`.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transfer(address to, uint256 value) external returns (bool);

    /**
     * @dev Returns the remaining number of tokens that `spender` will be
     * allowed to spend on behalf of `owner` through {transferFrom}. This is
     * zero by default.
     *
     * This value changes when {approve} or {transferFrom} are called.
     */
    function allowance(address owner, address spender) external view returns (uint256);

    /**
     * @dev Sets a `value` amount of tokens as the allowance of `spender` over the
     * caller's tokens.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * IMPORTANT: Beware that changing an allowance with this method brings the risk
     * that someone may use both the old and the new allowance by unfortunate
     * transaction ordering. One possible solution to mitigate this race
     * condition is to first reduce the spender's allowance to 0 and set the
     * desired value afterwards:
     * https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729
     *
     * Emits an {Approval} event.
     */
    function approve(address spender, uint256 value) external returns (bool);

    /**
     * @dev Moves a `value` amount of tokens from `from` to `to` using the
     * allowance mechanism. `value` is then deducted from the caller's
     * allowance.
     *
     * Returns a boolean value indicating whether the operation succeeded.
     *
     * Emits a {Transfer} event.
     */
    function transferFrom(address from, address to, uint256 value) external returns (bool);
}


// File contracts/test/lido/contracts/interfaces/IStETH8.sol

// SPDX-FileCopyrightText: 2021 Lido <info@lido.fi>

// Original license: SPDX_License_Identifier: GPL-3.0

// pragma solidity ^0.8.0;

interface IStETH8 is IERC20 {
    function getPooledEthByShares(uint256 _sharesAmount) external view returns (uint256);

    function getSharesByPooledEth(uint256 _pooledEthAmount) external view returns (uint256);

    function submit(address _referral) external payable returns (uint256);
}


// File @oldzeppelin/contracts/token/ERC20/extensions/IERC20Permit.sol@v4.9.0

// Original license: SPDX_License_Identifier: MIT
// OpenZeppelin Contracts (last updated v4.9.0) (token/ERC20/extensions/IERC20Permit.sol)

// pragma solidity ^0.8.0;

/**
 * @dev Interface of the ERC20 Permit extension allowing approvals to be made via signatures, as defined in
 * https://eips.ethereum.org/EIPS/eip-2612[EIP-2612].
 *
 * Adds the {permit} method, which can be used to change an account's ERC20 allowance (see {IERC20-allowance}) by
 * presenting a message signed by the account. By not relying on {IERC20-approve}, the token holder account doesn't
 * need to send a transaction, and thus is not required to hold Ether at all.
 */
interface IERC20Permit {
    /**
     * @dev Sets `value` as the allowance of `spender` over ``owner``'s tokens,
     * given ``owner``'s signed approval.
     *
     * IMPORTANT: The same issues {IERC20-approve} has related to transaction
     * ordering also apply here.
     *
     * Emits an {Approval} event.
     *
     * Requirements:
     *
     * - `spender` cannot be the zero address.
     * - `deadline` must be a timestamp in the future.
     * - `v`, `r` and `s` must be a valid `secp256k1` signature from `owner`
     * over the EIP712-formatted function arguments.
     * - the signature must use ``owner``'s current nonce (see {nonces}).
     *
     * For more information on the signature format, see the
     * https://eips.ethereum.org/EIPS/eip-2612#specification[relevant EIP
     * section].
     */
    function permit(
        address owner,
        address spender,
        uint256 value,
        uint256 deadline,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) external;

    /**
     * @dev Returns the current nonce for `owner`. This value must be
     * included whenever a signature is generated for {permit}.
     *
     * Every successful call to {permit} increases ``owner``'s nonce by one. This
     * prevents a signature from being used multiple times.
     */
    function nonces(address owner) external view returns (uint256);

    /**
     * @dev Returns the domain separator used in the encoding of the signature for {permit}, as defined by {EIP712}.
     */
    // solhint-disable-next-line func-name-mixedcase
    function DOMAIN_SEPARATOR() external view returns (bytes32);
}


// File contracts/test/lido/contracts/IWstETH8.sol

// SPDX-FileCopyrightText: 2021 Lido <info@lido.fi>

// Original license: SPDX_License_Identifier: GPL-3.0

/* See contracts/COMPILERS.md */
// pragma solidity ^0.8.0;


/**
 * @title StETH token wrapper with static balances.
 * @dev It's an ERC20 token that represents the account's share of the total
 * supply of stETH tokens. WstETH token's balance only changes on transfers,
 * unlike StETH that is also changed when oracles report staking rewards and
 * penalties. It's a "power user" token for DeFi protocols which don't
 * support rebasable tokens.
 *
 * The contract is also a trustless wrapper that accepts stETH tokens and mints
 * wstETH in return. Then the user unwraps, the contract burns user's wstETH
 * and sends user locked stETH in return.
 *
 * The contract provides the staking shortcut: user can send ETH with regular
 * transfer and get wstETH in return. The contract will send ETH to Lido submit
 * method, staking it and wrapping the received stETH.
 *
 */
interface IWstETH8 is IERC20Permit {
	function stETH() external view returns (IStETH8);

	/**
	 * @notice Exchanges stETH to wstETH
	 * @param _stETHAmount amount of stETH to wrap in exchange for wstETH
	 * @dev Requirements:
	 *  - `_stETHAmount` must be non-zero
	 *  - msg.sender must approve at least `_stETHAmount` stETH to this
	 *    contract.
	 *  - msg.sender must have at least `_stETHAmount` of stETH.
	 * User should first approve _stETHAmount to the WstETH contract
	 * @return Amount of wstETH user receives after wrap
	 */
	function wrap(uint256 _stETHAmount) external returns (uint256);

	/**
	 * @notice Exchanges wstETH to stETH
	 * @param _wstETHAmount amount of wstETH to uwrap in exchange for stETH
	 * @dev Requirements:
	 *  - `_wstETHAmount` must be non-zero
	 *  - msg.sender must have at least `_wstETHAmount` wstETH.
	 * @return Amount of stETH user receives after unwrap
	 */
	function unwrap(uint256 _wstETHAmount) external returns (uint256);

	// /**
	// * @notice Shortcut to stake ETH and auto-wrap returned stETH
	// */
	// receive() external payable {
	//     uint256 shares = stETH.submit{value: msg.value}(address(0));
	//     _mint(msg.sender, shares);
	// }

	/**
	 * @notice Get amount of wstETH for a given amount of stETH
	 * @param _stETHAmount amount of stETH
	 * @return Amount of wstETH for a given stETH amount
	 */
	function getWstETHByStETH(uint256 _stETHAmount) external view returns (uint256);

	/**
	 * @notice Get amount of stETH for a given amount of wstETH
	 * @param _wstETHAmount amount of wstETH
	 * @return Amount of stETH for a given wstETH amount
	 */
	function getStETHByWstETH(uint256 _wstETHAmount) external view returns (uint256);

	/**
	 * @notice Get amount of stETH for a one wstETH
	 * @return Amount of stETH for 1 wstETH
	 */
	function stEthPerToken() external view returns (uint256);

	/**
	 * @notice Get amount of wstETH for a one stETH
	 * @return Amount of wstETH for a 1 stETH
	 */
	function tokensPerStEth() external view returns (uint256);
}
